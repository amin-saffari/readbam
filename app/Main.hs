{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE ForeignFunctionInterface #-}

module Main where

--import qualified Text.Printf as TP
import qualified Options.Generic as OG
import qualified Bio.Bam         as BB
import qualified Bio.Base        as BBa
import qualified System.IO       as SI
--import Pipes
import qualified Pipes.Prelude   as Pipes
import qualified Data.ListLike   as LL

import Foreign
import Foreign.C.Types

data CommandLine = CommandLine
     {
         inputFile  :: FilePath
     ,   outputFile :: FilePath
     } deriving (OG.Generic, Show)

instance OG.ParseRecord CommandLine

data Region = Region {
    chr    :: BBa.Seqid ,
    from   :: Int ,
    to     :: Int ,
    strand :: String 
}

instance Eq Region where
  (Region c1 f1 t1 s1) == (Region c2 f2 t2 s2) = 
         (c1 == c2)
      && (f1 == f2) 
      && (t1 == t2)
      && (s2 == s2)

instance Show Region where
  show (Region chr pos_s pos_end strand) = BBa.unpackSeqid chr 
      ++ "\t" ++ 
      show pos_s 
      ++ "\t" ++
      show pos_end 
      ++ "\t" ++ 
      show strand

findOverlap :: Region -> Region -> Maybe Region
findOverlap r0 r1 
      | chr r0 == chr r1 && from r1 <= to r0 = 
          Just $ Region (chr r0) (min (from r0) (from r1)) (max (to r0) (to r1)) (strand r0)
      | otherwise                            = Nothing

-- Finding exon by overlaping reads
-- TODO: number of reads which participated in every exons 
-- also should be reported.
overlaps :: (Monad m, BB.Nullable s,BB.ListLike s Region) => BB.Enumeratee s s m b
overlaps out = do mr <- BB.tryHead   -- mr :: Maybe Region
                  case mr of Nothing -> return out
                             Just r0 -> BB.eneeCheckIfDone (go r0) out
               where
                   go r0 k = do 
                       mr <- BB.tryHead  -- mr :: Maybe Region
                       case mr of 
                          Nothing -> return $ k $ BB.Chunk $ LL.singleton r0
                          Just r1 -> 
                            case findOverlap r0 r1 of
                               Nothing -> BB.eneeCheckIfDone (go r1) . k . BB.Chunk $ LL.singleton r0
                               Just rr -> go rr k

info ::BB.Refs -> BB.BamRaw -> Region
info rs r | BB.isReversed rec = Region (ref_name rec) (BB.b_pos rec) (BB.b_pos rec + BB.extAsInt l "XY" rec) "-" -- Be careful you are 1 base off
          | otherwise         = Region (ref_name rec) (BB.b_pos rec) (BB.b_pos rec + BB.extAsInt l "XY" rec) "+"
        where 
          rec      = BB.unpackBam r
          ref_name a = BB.sq_name $ BB.getRef rs $ BB.b_rname a
          l        = length .show $ BB.b_seq rec

transcriptRegionRepresentation :: Region -> String
transcriptRegionRepresentation (Region chr pos_s pos_end strand) = BBa.unpackSeqid chr
           ++ "\t" ++
           show pos_s
           ++ "\t" ++
           show pos_end 
           ++ "\t" ++
           strand


data Region2 = Region2 String Int Int Int Int

-- I don't want transcriptRegion in my print so I need to either override show or 
-- explicitly represent transcriptRegion as normal String 
-- 1st I go for show
instance Show Region2 where
   show (Region2 chr pos_s pos_end ali_len seq_len) = chr 
          ++ "\t" ++ 
          show pos_s 
          ++ "\t" ++
          show pos_end 
          ++ "\t" ++ 
          show ali_len 
          ++ "\t" ++ 
          show seq_len

-- represent transcriptRegion as normal String
-- This is really boilarplate! I can use Generic programming and walk through 
-- each fields of records and applying a function to them! (gzipWithT in SYB)
-- But this is overkill in my case
transcriptRegionRepresentation2 :: Region2 -> String
transcriptRegionRepresentation2 (Region2 chr pos_s pos_end ali_len seq_len) = chr --TP.printf "%s" chr
           ++ "\t" ++ 
           show pos_s 
           ++ "\t" ++
           show pos_end 
           ++ "\t" ++ 
           show ali_len 
           ++ "\t" ++ 
           show seq_len

-- I can't abstract show like this as:
-- 1) Fields don't have the same type
-- 2) Applying show to an string doesn't produce nice result \"
transcriptRegionRepresentation2' :: Region2 -> String
transcriptRegionRepresentation2' (Region2 chr pos_s pos_end ali_len seq_len)
       = chr ~~ pos_s ~~ pos_end ~~ ali_len ~~ seq_len ~~ ""
     where
       a ~~ x = show a ++ "\t" ++ show x  


info3 :: BB.Refs -> BB.BamRaw -> Region2
info3 rs r 
  |  BB.isReversed rec = Region2 (BBa.unpackSeqid $ BB.sq_name $ BB.getRef rs $ BB.b_rname rec) (BB.b_pos rec) (BB.b_pos rec + BB.extAsInt p "XY" rec) l p
  | otherwise          = Region2 (BBa.unpackSeqid $ BB.sq_name $ BB.getRef rs $ BB.b_rname rec) (BB.b_pos rec) (BB.b_pos rec + BB.extAsInt p "XY" rec) l p
         where rec = BB.unpackBam r
               l   = BB.alignedLength $ BB.b_cigar rec
               p   = length . show $ BB.b_seq rec

main :: IO ()
main = 
     OG.getRecord "Splice-aware bam to fastq" >>= \cmd ->
--   How about using Maybe to handle Standard handles!?
--        if isNothing (outputFile cmd) then stout 
--                                      else )
       SI.withFile (outputFile cmd) SI.WriteMode $ \hdl ->
       BB.decodeAnyBamFile (inputFile cmd) --decode input bam
       BB.>=> BB.run $ \hdr -> 
       BB.joinI $ BB.mapStream (info (BB.meta_refs hdr)) $
       BB.joinI $ overlaps $
       BB.mapStreamM_ $ SI.hPutStrLn hdl . show -- print is the same a $ putStrLn . show so why not hPrint

